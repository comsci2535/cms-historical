<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_database_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('setting_database a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('setting_database a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.database_type', $param['search']['value'])
                    ->or_like('a.hostname', $param['search']['value'])
                    ->or_like('a.username', $param['search']['value'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.database_type', $param['search']['value'])
                    ->or_like('a.hostname', $param['search']['value'])
                    ->or_like('a.username', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.hostname";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.username";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 6) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['is_default']) ):
            $this->db->order_by('a.is_default', 'DESC');
        endif;
        
        if ( isset($param['setting_database_id']) ) 
            $this->db->where('a.setting_database_id', $param['setting_database_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);

        if (!empty($param['database_type'])):
            $this->db->where('a.database_type', $param['database_type']);
        endif;
        
    }
    
    public function insert($value) {
        $this->db->insert('setting_database', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('setting_database_id', $id)
                        ->update('setting_database', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('setting_database_id', $id)
                        ->update('setting_database', $value);
        return $query;
    }    

}
