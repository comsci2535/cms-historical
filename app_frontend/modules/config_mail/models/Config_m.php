<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Config_m Extends CI_Model {
    
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_config($type=null)
    {
        if (isset($type))
            $this->db->where('type', $type);
        $query = $this->db
                        ->from('config')
                        ->get();
        return $query;
    }
    
    public function update($value, $type)
    {
        $this->db
                ->where('type', $type)
                ->delete('config');
        $rs = $this->db
                    ->insert_batch('config', $value);
        return $rs;
    }
    
    public function update_upload($value)
    {
        if (!empty($value)) {
        $rs = $this->db
                    ->update_batch('config', $value, 'variable');
        }else{
            $rs = TRUE;
        }
        return $rs;
    }
}

