<?php 

/**
 * 
 */
class Users_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function find_users_by_user($username){
		$this->_condition();
		$query = $this->db->where('username',$username)
		         ->get('users');
		return $query->row();         
	}

	public function find_users_by_user_and_salt($username,$salt){
		$this->_condition();
		$query = $this->db->where('username',$username)
						  ->where('salt',$salt)
		                  ->get('users');
		return $query->row();         
	}

	private function _condition() 
    {   
        
            $this->db->where('active', 1);
            $this->db->where('recycle', 0);


    }

	
}