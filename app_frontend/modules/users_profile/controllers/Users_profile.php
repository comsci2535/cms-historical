<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_profile extends MY_Controller
{
	private $_title = 'ข้อมูลส่วนตัว';
    private $_pageExcerpt = '';
    private $_permission;
    private $_grpContent = 'users';

	public function __construct()
	{
		parent::__construct();
		$this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
		$this->load->library('users_library');
		$this->load->library('logs_library');
		$this->load->library('encryption');
		$this->load->model("users_m");
	}

	public function index(){
        $this->load->module('template');
        
        $id = $this->session->users['user_id'];//decode_id($this->session->users['user_id']);
        $input['user_id'] = $id;
        $info = $this->users_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");
        $data['frmActionPassword'] = site_url("{$this->router->class}/update_password");
        $data['roles'] = Modules::run('roles/dropdown');
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/edit";
        
        $this->template->layout($data);
    }    

    
    
    public function update(){
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->users_m->update($id, $value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
     public function update_password() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['id']);
        
        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
        $value['password'] = $password;
        $value['salt'] = $salt;
        $result = $this->users_m->update($input['user_id'], $value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    
    
    private function _build_data($input){

        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        
       
        $value['email'] = $input['email'];
        $value['fname'] = $input['fname'];
        $value['lname'] = $input['lname'];
      
        $value['fullname'] = $input['fname'].' '.$input['lname'];
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['type'] = "admin";
            $value['password'] = $password;
            $value['salt'] = $salt;
            $value['username'] = $input['username'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function check_password() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['id']);
        $input['recycle'] = 0;
        $info = $this->users_m->get_rows($input);
        $row = $info->row();
      
        if($this->users_library->hash_password($input['oldPassword'],$row->salt) !== $row->password){
            $rs = FALSE;
        } else {
            $rs = TRUE;
           
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($rs));
    }

    
}
