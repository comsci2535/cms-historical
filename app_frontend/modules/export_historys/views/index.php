
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <form  role="form">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label text-right" for="">Database</label>
                    <div class="col-sm-3">
                        <input value="Oracle" type="text" class="form-control m-input " name="" id="" placeholder="ระบุ" required> 
                    </div> 
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label text-right" for="">Favorite Name</label>
                    <div class="col-sm-5"> 
                        <input value="" type="text" class="form-control m-input " name="" id="" placeholder="ระบุ" required> 
                    </div>  
                     
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-success m-btn--wide" style="width: 100%;margin-bottom: 10px;"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</button>  
                    </div>
                </div>
                <br>
            
                <table id="data-lists" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: left;">FieLd 1</th>
                            <th style="text-align: left;">FieLd 2</th>
                            <th style="text-align: left;">FieLd 3</th>
                            <th style="text-align: left;">FieLd 4</th>
                            <th style="text-align: left;">FieLd 5</th>
                            <th style="text-align: left;">FieLd 6</th> 
                            <th style="text-align: left;">FieLd 7</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i = 0; $i < 5; $i++):
                        ?>
                        <tr>
                            <td>FieLd 1</td>
                            <td>FieLd 2</td>
                            <td>FieLd 3</td>
                            <td>FieLd 4</td>
                            <td>FieLd 5</td>
                            <td>FieLd 6</td> 
                            <td><a href="javascript:void(0)">file<?=$i?>.txt</a></td>
                        </tr>
                         <?php
                         endfor;
                         ?>
                    </tbody>
                </table>
            </form>

        </div>
    </div>
</div>