"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            display_page: {
                required:true
            },
            excerpt: {
                required:true
            },
            detail: {
                required:true
            }
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    /* -- fileinput */
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }
    
    if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:1024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
