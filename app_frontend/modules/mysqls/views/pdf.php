<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$fileName?></title>
</head>
<body>
    <style>
        table {
            border-collapse: collapse;
        }

        table, td, th {
        border: 1px solid black;
        }
    </style>
        <table class="table table-hover dataTable" width="100%" border="1">
            <thead>
                <?php
                if(!empty($table_thead)): 
                ?>
                <tr>
                    <?php
                    foreach($table_thead as $thead):
                    ?>
                    <th style="text-align: left;"><?php echo !empty($thead) ? $thead : '';?></th>
                    <?php
                    endforeach;
                    ?>
                </tr>
                <?php 
                endif;
                ?>
            </thead>
            <tbody>
                <?php
                if(!empty($table_thead)): 
                    if(!empty($table_result)): 
                        foreach($table_result as $result):
                ?>
                        <tr>
                            <?php
                            foreach($table_thead as $thead):
                                $thead = !empty($thead) ? $thead : "";
                            ?>
                            <td><?php echo !empty($result->$thead) ? $result->$thead : '';?></td>
                            <?php
                            endforeach;
                            ?> 
                        </tr>
                <?php
                        endforeach;
                    endif; 
                endif;
                ?>
            </tbody>
        </table> 
    </body>
</html>