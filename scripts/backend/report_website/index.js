"use strict";

$(function () {
  report_monthly();
  report_daily();
  $(".daily-month, .daily-year").change(function () {
    report_daily();
  });

  $(".monthly-year").change(function () {
    report_monthly();
  });
});

function report_daily() {
  $.post("report_website/daily_device_data", {
    csrfToken: get_cookie("csrfCookie"),
    month: $(".daily-month").val(),
    year: $(".daily-year").val(),
  })
    .done(function (data) {
      daily_device_chart(data);
      $("#dialy-device-data").html(draw_table(data, "วันที่", "อุปกรณ์"));
    })
    .fail(function () {});
  $.post("report_website/daily_page_data", {
    csrfToken: get_cookie("csrfCookie"),
    month: $(".daily-month").val(),
    year: $(".daily-year").val(),
  })
    .done(function (data) {
      daily_page_chart(data);
      $("#dialy-page-data").html(draw_table(data, "วันที่", "หน้า"));
    })
    .fail(function () {});
}

function report_monthly() {
  $.post("report_website/monthly_device_data", {
    csrfToken: get_cookie("csrfCookie"),
    year: $(".monthly-year").val(),
  })
    .done(function (data) {
      monthly_device_chart(data);
      $("#monthly-device-data").html(draw_table(data, "เดือน", "อุปกรณ์"));
    })
    .fail(function () {});

  $.post("report_website/monthly_page_data", {
    csrfToken: get_cookie("csrfCookie"),
    year: $(".monthly-year").val(),
  })
    .done(function (data) {
      monthly_page_chart(data);
      $("#monthly-page-data").html(draw_table(data, "เดือน", "หน้า"));
    })
    .fail(function () {});
}

function daily_device_chart(response) {
  Highcharts.chart("daily-device", {
    chart: {
      type: "column",
    },
    title: {
      text:
        "รายงานเข้าชมเว็บไซต์รายเดือนประจำเดือน " +
        $(".daily-month option:selected").text() +
        " " +
        $(".daily-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามอุปกรณ์",
    },
    xAxis: {
      categories: response.day,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "normal",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>วันที่ {point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function daily_page_chart(response) {
  Highcharts.chart("daily-page", {
    chart: {
      type: "column",
    },
    title: {
      text:
        "รายงานเข้าชมเว็บไซต์รายเดือนประจำเดือน " +
        $(".daily-month option:selected").text() +
        " " +
        $(".daily-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามหน้าเข้าชม",
    },
    xAxis: {
      categories: response.day,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>วันที่ {point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function daily_department_chart(response) {
  Highcharts.chart("daily-department", {
    chart: {
      type: "column",
    },
    title: {
      text:
        "รายงานเข้าชมเว็บไซต์รายเดือนประจำเดือน " +
        $(".daily-month option:selected").text() +
        " " +
        $(".daily-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามแผนก",
    },
    xAxis: {
      categories: response.day,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>วันที่ {point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function monthly_device_chart(response) {
  Highcharts.chart("monthly-device", {
    chart: {
      type: "column",
    },
    title: {
      text: "รายงานเข้าชมเว็บไซต์รายเดือนประจำปี " + $(".monthly-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามอุปกรณ์",
    },
    xAxis: {
      categories: [
        "มค.",
        "กพ.",
        "มีค.",
        "มย.",
        "พค.",
        "มิย.",
        "กค.",
        "สค.",
        "กย.",
        "ตค.",
        "พย.",
        "ธค.",
      ],
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>{point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function monthly_page_chart(response) {
  Highcharts.chart("monthly-page", {
    chart: {
      type: "column",
    },
    title: {
      text: "รายงานเข้าชมเว็บไซต์รายเดือนประจำปี " + $(".monthly-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามหน้าเข้าชม",
    },
    xAxis: {
      categories: [
        "มค.",
        "กพ.",
        "มีค.",
        "มย.",
        "พค.",
        "มิย.",
        "กค.",
        "สค.",
        "กย.",
        "ตค.",
        "พย.",
        "ธค.",
      ],
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>เดือน {point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function monthly_department_chart(response) {
  Highcharts.chart("monthly-department", {
    chart: {
      type: "column",
    },
    title: {
      text: "รายงานเข้าชมเว็บไซต์รายเดือนประจำปี " + $(".monthly-year").val(),
    },
    subtitle: {
      text: "แบ่งแยกตามแผนก",
    },
    xAxis: {
      categories: [
        "มค.",
        "กพ.",
        "มีค.",
        "มย.",
        "พค.",
        "มิย.",
        "กค.",
        "สค.",
        "กย.",
        "ตค.",
        "พย.",
        "ธค.",
      ],
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "จำนวนหน้าเข้าชม",
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color: (Highcharts.theme && Highcharts.theme.textColor) || "gray",
        },
        formatter: function () {
          if (this.total == 0) {
            return "";
          } else {
            return this.total;
          }
        },
      },
    },
    tooltip: {
      headerFormat: "<b>เดือน {point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            fontWeight: "normal",
          },
          color:
            (Highcharts.theme && Highcharts.theme.dataLabelsColor) || "white",
        },
      },
    },
    series: response.series,
  });
}

function draw_table(data, x, y) {
  $html =
    '<table class="table table-bordered table-hover" style="margin-bottom:50px">';
  $html += "<thead>";
  $html +=
    '<tr><th rowspan="2" valign="middle">' +
    y +
    '</th><th class="text-center" colspan="' +
    data.day.length +
    '" >' +
    x +
    "</th></tr>";
  $html += "<tr>";
  $.each(data.day, function (index, data) {
    $html += "<th>" + data + "</th>";
  });
  $html += "</tr>";
  $html += "</thead>";
  $html += "<tbody>";
  var sum = [];
  $.each(data.series, function (index, data) {
    $html += "<tr>";
    $html += "<td>" + data.name + "</td>";
    $.each(data.data, function (index, data) {
      if (sum[index] == undefined) {
        sum[index] = data;
      } else {
        sum[index] = sum[index] + data;
      }
      if (data == 0) data = "-";
      $html += "<td>" + data + "</td>";
    });
    $html += "</tr>";
  });
  $html += "<tr>";
  $html += "<td>รวม</td>";
  $.each(sum, function (index, data) {
    if (data == 0) data = "-";
    $html += "<td>" + data + "</td>";
  });
  $html += "</tr>";
  $html += "</tbody>";
  $html += "</table>";
  console.log(sum);
  return $html;
}

$(window).on("load", function () {});

$(window).on("scroll", function () {});
