"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true
        }
    });

     // form validate editor
     $('#btn-test-connect').click(function(){
        var url = controller+'/api_test_connect';
        $.ajax({
            url     : url,
            type    : 'POST',
            dataType: 'json',
            headers : {'X-CSRF-TOKEN': csrfToken},
            data    : $('form.frm-create').serialize(),
        })
        .done(function (data) {
            if (data.success === true) {
                  toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
            } else if (data.success === false) {
                  $('#overlay-box').addClass('hidden');
                  toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
            }
          })
          .fail(function () {
              toastr["error"]("ไม่สามารถเชื่อมต่อฐานข้อมูลได้ กรุณาลองใหม่อีกครั้ง...", "");
          })
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
