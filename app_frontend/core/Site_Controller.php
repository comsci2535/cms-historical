<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* The MX_Controller class is autoloaded as required */
class Site_Controller extends MX_Controller
{
	function __construct() {
		parent::__construct();
	}
	
	protected function pagin($uri, $total, $segment, $per_page = 30, $num_links = 1) {
        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
		$config['num_links'] = $num_links;
		$config['full_tag_open'] = '<nav class="text-right"><ul class="pagination"><li>';
		$config['full_tag_close'] = '</li></ul></nav>';
 		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['first_link'] = false;
		$config['last_link'] = false;
	 	$config['next_link'] = 'Next »';
		$config['prev_link'] = '« Priveuos';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
	public function webfile()
	{
	  //file strore image and doc
		$webfile_url_=base_url();
		return $webfile_url_;
	}

	public function pathfile()
	{
	  //file strore image and doc
		$webfile_url_=FCPATH;
		return $webfile_url_;
	}
 
}
/*END*/
