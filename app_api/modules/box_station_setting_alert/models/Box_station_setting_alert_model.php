<?php

/**
 * 
 */
class Box_station_setting_alert_model extends CI_Model
{    
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

	public function get_rows($param) 
    {
        $this->_condition($param); 
        
        $query = $this->db
                        ->select('a.*')
                        ->from('box_station_setting_alert a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('box_station_setting_alert a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
                
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }      
        
        if (!empty($param['box_station_user_map_id']) ) 
            $this->db->where('a.box_station_user_map_id', $param['box_station_user_map_id']);
            
        if (isset($param['alert_line']) ) 
            $this->db->where('a.alert_line', $param['alert_line']);

        if (isset($param['alert_email']) )
            $this->db->where('a.alert_email', $param['alert_email']);

    }
}

?>