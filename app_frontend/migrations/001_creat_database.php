<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_creat_database extends CI_Migration {

    public $CI;

    public function up()
    {
        $this->CI =& get_instance();
       

        /**
         * users
        */
         $this->dbforge->add_field(array(
           'user_id' => array(
               'type' => 'INT',
               'constraint' => 5,
               'unsigned' => TRUE,
               'auto_increment' => TRUE
           ),
           'username' => array(
               'type' => 'VARCHAR',
               'constraint' => 50,
           ),
           'password' => array(
               'type' => 'VARCHAR',
               'constraint' => 255,
           ),
           'salt' => array(
               'type' => 'VARCHAR',
               'constraint' => 255,
           ),
           'fname' => array(
               'type' => 'VARCHAR',
               'constraint' => 50,
           ),
           'lname' => array(
               'type' => 'VARCHAR',
               'constraint' => 50,
           ),
           'fullname' => array(
               'type' => 'VARCHAR',
               'constraint' => 100,
           ),
           'created_by' => array(
               'type' => 'VARCHAR',
               'constraint' => 100,
           ),
           'avatar' => array(
               'type' => 'VARCHAR',
               'null' => TRUE,
               'constraint' => 255,
           ),
           'email' => array(
               'type' => 'VARCHAR',
               'null' => TRUE,
               'constraint' => 50,
           ),
           'phone' => array(
               'type' => 'VARCHAR',
               'null' => TRUE,
               'constraint' => 20,
           ),
           'role_id' => array(
               'type' => 'INT',
               'constraint' => 11,
           ),
           'created_at' => array(
               'type' => 'DATETIME',
           ),
           'updated_at' => array(
               'type' => 'DATETIME',
           ),

       ));
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('users');   

        /**
         * [insert users]
         */
        $data = array(
            'username' => 'admin',
            'password' => '38a142d6f6a7b889587a97379cdc0775334169ee',
            'salt' => 'gmcZSu9Ce5DhZWl1w.bZ3e',
            'fname' => 'super',
            'lname' => 'admin',
            'fullname' => 'ADMIN.S',
            'created_by' => 'ADMIN.S',
            'avatar' => '',
            'email' => 'admin@admin.com',
            'phone' => '',
            'role_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $this->CI->db->insert('users', $data);

    }// up


    public function down()
    {

        $this->dbforge->drop_table('users');
    }// down


}