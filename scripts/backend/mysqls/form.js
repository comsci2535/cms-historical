"use strict";

$(document).ready(function () {
  $(".frm-create").validate({
    rules: {
      title: true,
      slug: true,
      managearticle_id: {
        required: true,
      },
      excerpt: {
        required: true,
      },
      file: {
        required: true,
      },
    },
  });
});

$(window).on("load", function () {});

$(window).on("scroll", function () {});
