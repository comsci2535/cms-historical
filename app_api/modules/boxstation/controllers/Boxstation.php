<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed'); 
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php'; 

class Boxstation extends REST_Controller 
{     
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('boxstation_model');
        $this->load->model('line_notify/line_notify_model'); 
        $this->load->model('box_station_setting_alert/box_station_setting_alert_model'); 
        $this->load->library('line_notify');
        $this->load->helper('utils_api_helper');
    }

	public function add_post()
    { 
          
        $StationName = $this->post('StationName');
        $Data_obj    = $this->post('Data');

        $param['box_station_code'] = $StationName;
        $param['active']           = 1; 
        $info = $this->boxstation_model->get_rows($param)->row();
        if(!empty($info)): 
            $title              = !empty($info->title) ? $info->title : $StationName;
            $user_id            = !empty($info->user_id) ? $info->user_id : 'system';
            $param_['user_id']  = $user_id;
            $param_['active']   = 1;

            $param__['active']  = 1;
            $param__['box_station_user_map_id']  = !empty($info->box_station_user_map_id) ? $info->box_station_user_map_id : 0;  
            $info_setting_alert = $this->box_station_setting_alert_model->get_rows($param__)->row();

            if(!empty($Data_obj)):
                foreach ($Data_obj as $value):

                    if(!empty($info_setting_alert)):
                        if(!empty($info_setting_alert->alert_line)):

                            $info_notify = $this->line_notify_model->get_rows($param_)->row();  
                            if($value['Humidity'] < $info_setting_alert->humidity_low): 
                                $message ='ความชื้นต่ำสุดอยู่ที่ '.$value['Humidity'].' < '.$info_setting_alert->humidity_low.' เปิดปั้มน้ำ (On)';
                                if(!empty($info_notify)): 
                                    $notify['token']    = !empty($info_notify->token) ? $info_notify->token :'';
                                    $notify['line_url'] = !empty($info_notify->line_url) ? $info_notify->line_url :'';
                                    $notify['message']  = $message;
                                    // alert notify to line api
                                    $this->line_notify->notify($notify);
                                endif;  
                            else:
                                if($value['Humidity'] < $info_setting_alert->humidity_max):  
                                    if($value['temperature'] > $info_setting_alert->temperature_max): 
                                        $message ='ความชื้นมากสุดอยู่ที่ '.$value['Humidity'].' < '.$info_setting_alert->humidity_max.' และ '.$value['temperature'].' > '.$info_setting_alert->temperature_max.' เปิดปั้มน้ำ (On)';   
                                        if(!empty($info_notify)): 
                                            $notify['token']    = !empty($info_notify->token) ? $info_notify->token :'';
                                            $notify['line_url'] = !empty($info_notify->line_url) ? $info_notify->line_url :'';
                                            $notify['message']  = $message;
                                            // alert notify to line api
                                            $this->line_notify->notify($notify);
                                        endif; 
                                    endif; 
                                endif;
                            endif; 
                        endif; 
                    endif; 

                    $data_arr = array(
                        'title'             => $StationName
                        ,'box_station_code' => $StationName
                        ,'humidity'         => $value['Humidity']
                        ,'temperature'      => $value['temperature']
                        ,'created_by'       => $user_id
                        ,'updated_by'       => $user_id
                        ,'active'           => 1
                        ,'created_at'       => db_datetime_now()
                        ,'updated_at'       => db_datetime_now()
                    ); 
                    $this->boxstation_model->save($data_arr);
                endforeach;
            endif;
            
        endif;
        $data['status'] = status(200); 
        $this->response($data, REST_Controller::HTTP_OK);
    }


}
