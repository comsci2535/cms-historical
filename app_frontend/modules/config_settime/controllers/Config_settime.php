<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Config_settime extends MX_Controller {

    private $_title = 'ตั้งค่า Job';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับตั้งค่า Job';
    private $_grpContent = 'settime';
    private $_permission;

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if (!$this->_permission && !$this->input->is_ajax_request()) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('uploadfile_library');
        $this->load->model('config_settime_m');
    }

    public function index() 
    {
        $this->load->module('template');

        $data['frmAction']      = site_url("{$this->router->class}/update");
        $data['job_type']       = 'settime';
        $info   = $this->config_settime_m->get_rows($data)->row();
        $data['info']           = $info;
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/{$this->router->method}";
        $data['pageScript'] = "scripts/backend/config_settime/form.js";
        $data['breadcrumb'][] = array($this->_title, "javascript:void(0)"); 
        
        $this->template->layout($data);
    }

    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = $input['job_type'];
        $value  = $this->_build_data($input);
        $info   = $this->config_settime_m->get_rows($input)->row();
       
        if(!empty($info)):
            $result = $this->config_settime_m->update($id, $value);
        else:
            $result = $this->config_settime_m->insert($value);
        endif;
        
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['job_type']      = $input['job_type'];
        $value['job_day']       = $input['job_day'];
        $value['job_week']      = $input['job_week'];
        $value['job_month']     = $input['job_month'];

        $path = 'setting_time';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        arr($upload);
        $file = '';
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
			if(isset($outfile)){
				$this->load->helper("file");
				@unlink($outfile);
            }
            $value['file'] = $file;
        }

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function run_file()
    {
        $rootPath   ='uploads/setting_time/2020/10/70d865d5a9ed39ae566705b00780f9d0.sh';
        $rootGetcwd = getcwd().'/';
        $full_path  = $rootGetcwd.$rootPath;
        echo $rootGetcwd;
        echo '<br>';
        echo $rootPath;
        echo '<br>';
        echo 'Full path => '.$full_path;
        echo '<pre>';
        //echo exec('ls -l');
        // mysql -u root -ppassword -h localhost -D db_historical -e  "INSERT INTO testupdatescriptdemo (date) VALUES (NOW());"
        //echo shell_exec('mysql -u root -proot -h localhost -D db_historical -e "INSERT INTO testupdatescriptdemo (date) VALUES (NOW());"');
        // $cmd = escapeshellcmd('mysql -u root -p root -h localhost -D db_historical -e "show databases;"');
        // $cmd = escapeshellcmd('ls -l');
        echo exec('mysql --user=root --password=root -e "show databases;"');
        echo '</pre>';
        exit(0);
    }

}
