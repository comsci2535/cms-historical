<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body">
             <div class="form-group m-form__group row m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">ติดต่อเรา</h4></label>
                <div class="col-sm-7"></div>
            </div>  
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >Email</label>
                <div class="col-sm-8">
                    <input value="<?php echo isset($info['email']) ? $info['email'] : NULL ?>" type="text" id="" class="form-control" name="email">
                </div>
            </div>  
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">Facebook</label>
                <div class="col-sm-8">
                     <input value="<?php echo isset($info['facebook']) ? $info['facebook'] : NULL ?>" type="text" id="" class="form-control" name="facebook">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">Line</label>
                <div class="col-sm-8">
                     <input value="<?php echo isset($info['line']) ? $info['line'] : NULL ?>" type="text" id="" class="form-control" name="line">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                <div class="col-sm-8">
                    <textarea name="detail" rows="5" class="form-control" id="detail" placeholder="ระบุ"><?php echo isset($info['detail']) ? $info['detail'] : NULL ?></textarea>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">เบอร์โทรศัพท์</label>
                <div class="col-sm-3">
                    <input value="<?php echo isset($info['phone']) ? $info['phone'] : NULL ?>" type="text" id="" class="form-control" name="phone">
                </div>
                <label class="col-sm-2 col-form-label">เวลาติดต่อ</label>
                <div class="col-sm-3">
                    <input value="<?php echo isset($info['time']) ? $info['time'] : NULL ?>" type="text" id="" class="form-control" name="time">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">Latitude</label>
                <div class="col-sm-3">
                    <input value="<?php echo isset($info['latitude']) ? $info['latitude'] : NULL ?>" type="text" id="" class="form-control" name="latitude">
                </div>
                <label class="col-sm-2 col-form-label">Longitude</label>
                <div class="col-sm-3">
                    <input value="<?php echo isset($info['longitude']) ? $info['longitude'] : NULL ?>" type="text" id="" class="form-control" name="longitude">
                </div>
            </div>
                            
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>


