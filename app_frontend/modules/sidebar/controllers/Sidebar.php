<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends MX_Controller {
    
    public $allParent;

    public function __construct() {
        parent::__construct();
        $this->load->model("sidebar_m");
        
    }

    public function gen() {
        //arr($this->router->class);exit();
        $sidebar = array();
        $allModuleId = array();
        $info = $this->sidebar_m->get_sidebar();
        $user = $this->session->users;
       // arr($info->result());exit();
        $roles = Modules::run('roles/login', $user['RoleId']);
        $user['roles'] = $roles;
        $this->session->set_userdata('users', $user);
        $module = $this->sidebar_m->get_module();
        foreach ( $module->result() as $rs ) {
            $modules[$rs->moduleId] = $rs;
        }   
        if ( !empty($roles) ) {
            foreach ( $roles as $key => $rs ) {
                $this->allParent = array();
                $allParent = $this->_all_parent($key, $modules);
                $allModuleId = array_merge($allModuleId ,$allParent);
            }
        }
        foreach ( $info->result() as $rs ) {
            if ( $this->session->users['type'] == 'developer' || in_array($rs->moduleId, $allModuleId) ) {
                $href = site_url() . $rs->class;
                if ( $rs->method != "" && $rs->method !== "index" ) {
                    $href = $href . '/' . $rs->method;
                }

                $active = FALSE;
                if ( $rs->class == $this->router->class ) {
                    $active = TRUE;
                }

                if ( $rs->class == 'boxs' && $this->router->class =='box_station' ) {
                    $active = TRUE;
                } 
                
                
                $thisref = &$refs[$rs->moduleId];
                $thisref['target'] = "";
                if(substr($rs->class,0,4)=="http"){ 
                    $href = $rs->class;
                    $thisref['target'] = "_blank";
                }
                $thisref['moduleId'] = $rs->moduleId;
                $thisref['parentId'] = $rs->parentId;
                $thisref['title'] = $rs->title;
                $thisref['icon'] = $rs->icon;
                $thisref['type'] = $rs->type;
                $thisref['href'] = $href;
                $thisref['active'] = $active;
                if ( $rs->parentId != 0 ) {
                    if ( $active ) {
                        $refs[$rs->parentId]['active'] = true;
                        if ( isset($refs[$refs[$rs->parentId]['parentId']]) ) {
                           $refs[$refs[$rs->parentId]['parentId']]['active'] = true;
                        }
                    }
                    $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
                } else {
                    $sidebar[$rs->moduleId] = &$thisref;
                } 
            }
        }
  //arrx($sidebar);
        return $sidebar;
    }
    
    private function _all_parent($key, $module){
        if ( isset($module[$key]) ) {
            if ( $module[$key]->parentId != 0 ) {
                array_push($this->allParent, $module[$key]->parentId, $module[$key]->moduleId);
                $this->_all_parent($module[$key]->parentId, $module);
            } else {
                array_push($this->allParent, $module[$key]->moduleId);
            }
        } else {
            $this->allParent = array();
        }
        return $this->allParent;
    }

}
