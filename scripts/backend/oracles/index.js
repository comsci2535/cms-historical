"use strict";
$(document).ready(function () {
  dataList
    .DataTable({
      //language: {url: "assets/bower_components/datatables.net/Thai.json"},
      serverSide: true,
      ajax: {
        url: controller + "/data_index",
        type: "POST",
        data: { csrfToken: get_cookie("csrfCookie") },
      },
      order: [[1, "asc"]],
      pageLength: 10,
      columns: [
        {
          data: "checkbox",
          width: "20px",
          className: "text-center",
          orderable: false,
        },
        { data: "title", className: "", orderable: true },
        { data: "excerpt", className: "", orderable: true },
        { data: "created_at", width: "100px", className: "", orderable: true },
        { data: "updated_at", width: "100px", className: "", orderable: true },
        {
          data: "active",
          width: "50px",
          className: "text-center",
          orderable: false,
        },
        {
          data: "action",
          width: "30px",
          className: "text-center",
          orderable: false,
        },
      ],
    })
    .on("draw", function () {
      $(".bootstrapToggle").bootstrapToggle(bootstrapToggleOpt);
      $(".tb-check-single").iCheck(iCheckboxOpt);
    })
    .on("processing", function (e, settings, processing) {
      if (processing) {
        $("#overlay-box").removeClass("hidden");
      } else {
        $("#overlay-box").addClass("hidden");
      }
    });

  $(document).on("change", "#select-database-list", function () {
    // $('#frm-submit-data').submit();
  });

  $(document).on("change", "#select-tables-list", function () {
    var $this = $(this);
    console.log($this.val());
    $("#textarea-select-tables").val("select * from " + $this.val());
  });

  $(".frm-create").validate({
    rules: {
      setting_database_id: true,
      mysqltables: true,
      textarea_select_tables: true,
    },
  });

  $(document).on("change", "#select-database-list", function () {
    var $this = $(this);
    $.ajax({
      type: "POST",
      url: controller + "/database_lists",
      dataType: "json",
      async: false,
      data: {
        setting_database_id: $this.val(),
        csrfToken: get_cookie("csrfCookie"),
      },
      success: function (result) {
        if (result.success) {
          var html = "<option value=''>เลือกตาราง</option>";
          $.each(result.mysqltables, function (key, value) {
            console.log("value", value);
            html += "<option value='" + value + "'>" + value + "</option>";
          });
          $("select#select-tables-list").html(html);
          $(".select2").select2();
        } else {
          toastr[result.toastr.type](
            result.toastr.lineTwo,
            result.toastr.lineOne
          );
        }
        console.log("result", result);
      },
    }).fail(function () {
      toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "");
    });
  });

  $(document).on("click", "#btn-query-sql-export", function () {
    $(".frm-create-query-export").submit();
  });
});

$(window).on("load", function () {});

$(window).on("scroll", function () {});
