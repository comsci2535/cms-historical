<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body"> 
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="job_day">รันทุกๆ 1 วัน</label>
                <div class="col-sm-7">
                    <label class="m-checkbox m-checkbox--brand" style="margin-top: 10px;">
                        <input <?php if(!empty($info->job_day) &&  $info->job_day=='on'){ echo "checked";}?> type="checkbox" class="input-check-box" name="job_day" valus="1"> เปิด
                        <span></span>
                    </label> 
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="job_week">รันทุกๆ 1 สัปดาห์</label>
                <div class="col-sm-7">
                    <label class="m-checkbox m-checkbox--brand" style="margin-top: 10px;">
                        <input <?php if(!empty($info->job_week) &&  $info->job_week=='on'){ echo "checked";}?> type="checkbox" class="input-check-box" name="job_week" valus="1"> เปิด
                        <span></span>
                    </label> 
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="job_month">รันทุกๆ 1 เดือน</label>
                <div class="col-sm-7">
                    <label class="m-checkbox m-checkbox--brand" style="margin-top: 10px;">
                        <input <?php if(!empty($info->job_month) &&  $info->job_month=='on'){ echo "checked";}?> type="checkbox" class="input-check-box" name="job_month" valus="1"> เปิด
                        <span></span>
                    </label> 
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="file">File Setting</label>
                <div class="col-sm-7">
                    <input id="file" name="file" type="file" data-preview-file-type="text">
                        <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                </div>
            </div>          
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <input type="hidden" name="job_type" value="<?php echo !empty($job_type)? $job_type : ''; ?>">
                        <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>
<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : 'file'; ?>';
    var file_id         = '<?=$job_type;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$job_type ;?>'; 
</script>
