<style type="text/css">
    .hidden {
        display: none!important;
    }
    .policy tr.separator {background-color:#e6eeff;font-weight: bold}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">กลุ่มผู้ดูแล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->name) ? $info->name : NULL ?>" type="text" class="form-control" name="name" required>
                    </div>
                </div>      
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">อธิบาย</label>
                    <div class="col-sm-7">
                        <textarea name="remark" rows="3" class="form-control"><?php echo isset($info->remark) ? $info->remark : NULL ?></textarea>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-1 col-form-label" for="title">สิทธิ</label>
                    <div class="col-sm-10">
                        <table class="table table-bordered policy" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-left" width="50%">รายการ</th>
                                    <?php foreach ($permission as $key => $rs) { ?>
                                        <th class="text-center"><?=$rs->name;?></th>
                                    <?php } ?>
                                    <th class="text-center">ทั้งหมด</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($module as $level0) : ?>
                                    <?php if ( $level0['type'] == 2) : ?>
                                    <tr id="<?php echo $level0['moduleId'] ?>" class="<?php echo $level0['type'] == 2 ? "separator" : NULL ?>">
                                        <td class="text-center"><?php echo $level0['title'] ?></td>
                                       
                                          <?php foreach ($permission as $key => $rs) { ?>
                                            <td class="text-center">
                                                <?php if($rs->permission_id==1){ ?>
                                                    <input  <?php if(!empty($policy[$level0['moduleId']]) && in_array($rs->permission_id,$policy[$level0['moduleId']])){ echo "checked"; } ?>  type="checkbox" name="policy[<?php echo $level0['moduleId'] ?>][<?=$rs->permission_id;?>]" class="icheck access">
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                        <td class="text-left" ></td>
                                    </tr>
                                    <?php else : ?>
                                    <tr id="<?php echo $level0['moduleId'] ?>" class="">
                                        <td <?php echo isset($level0['children']) ? "colspan='".(count($permission)+2)."' style='background-color:#eee'" : null; ?>><?php echo $level0['title'] ?>
                                        </td>
                                        <?php if (!isset($level0['children'])) : ?>  
                                            <?php if ( $level0['type'] != 2 ) : ?>
                                            <?php foreach ($permission as $key => $rs) { ?>
                                                <td class="text-center">
                                                <input <?php if(!empty($policy[$level0['moduleId']]) && in_array($rs->permission_id,$policy[$level0['moduleId']])){ echo "checked"; } ?> type="checkbox" name="policy[<?php echo $level0['moduleId'] ?>][<?=$rs->permission_id;?>]" class="icheck access">
                                                </td>
                                            <?php } ?>
                                            <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                            <?php endif; ?>
                                        <?php endif; ?>                            
                                    </tr>
                                    <?php if (isset($level0['children'])) : ?>
                                        <?php foreach ($level0['children'] as $level1) : ?>
                                            <tr id="<?php echo $level1['moduleId'] ?>">
                                                <td <?php //echo isset($level1['children']) ? "colspan='4'" : null; ?> style="padding-left: 25px;"><?php echo $level1['title'] ?></td>
                                                <?php //if (!isset($level1['children'])) : ?> 
                                                    
                                                    <?php foreach ($permission as $key => $rs) { ?>
                                                        <td class="text-center">
                                                            <input <?php if(!empty($policy[$level1['moduleId']]) && in_array($rs->permission_id,$policy[$level1['moduleId']])){ echo "checked"; } ?>  type="checkbox" name="policy[<?php echo $level1['moduleId'] ?>][<?=$rs->permission_id;?>]" class="icheck access">
                                                        </td>
                                                    <?php } ?>
                                                    <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                <?php //endif; ?>
                                            </tr>  
                                            <?php if (isset($level1['children'])) : ?>
                                                <?php foreach ($level1['children'] as $level2) : ?>
                                                    <tr id="<?php echo $level2['moduleId'] ?>">
                                                        <td <?php echo isset($level2['children']) ? "colspan='4'" : null; ?> style="padding-left: 45px"><?php echo $level2['title'] ?></td>
                                                        <?php if (!isset($level2['children'])) : ?> 
                                                            <?php foreach ($permission as $key => $rs) { ?>
                                                                <td class="text-center">
                                                                    <input <?php if(!empty($policy[$level2['moduleId']]) && in_array($rs->permission_id,$policy[$level2['moduleId']])){ echo "checked"; } ?>   type="checkbox" name="policy[<?php echo $level2['moduleId'] ?>][<?=$rs->permission_id;?>]" class="icheck access">
                                                                </td>
                                                            <?php } ?>
                                                            <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                        <?php endif; ?>
                                                    </tr>  
                                                    <?php if (isset($level2['children'])) : ?>
                                                        <?php foreach ($level2['children'] as $level3) : ?>
                                                            <tr id="<?php echo $level3['moduleId'] ?>">
                                                                <td <?php echo isset($level3['children']) ? "colspan='4'" : null; ?> style="padding-left: 65px"><?php echo $level3['title'] ?></td>
                                                                <?php if (!isset($level3['children'])) : ?>
                                                                    <?php foreach ($permission as $key => $rs) { ?>
                                                                        <td class="text-center">
                                                                            <input <?php if(!empty($policy[$level3['moduleId']]) && in_array($rs->permission_id,$policy[$level3['moduleId']])){ echo "checked"; } ?>  type="checkbox" name="policy[<?php echo $level3['moduleId'] ?>][<?=$rs->permission_id;?>]" class="icheck access">
                                                                        </td>
                                                                    <?php } ?>
                                                                    <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                                <?php endif; ?>
                                                            </tr>  
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>                                          
                                                <?php endforeach; ?>
                                            <?php endif; ?>                                
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>                        
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->role_id) ? decode_id($info->role_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>



