<div class="col-md-7">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="is_default">ตั้งเป็นค่าเริ่มต้น</label>
                    <div class="col-sm-9">
                        <label class="m-checkbox m-checkbox--brand" style="margin-top: 10px;">
                            <input <?php if(!empty($info->is_default) &&  $info->is_default=='1'){ echo "checked";}?> type="checkbox" class="input-check-box" name="is_default" valus="1"> ตั้ง
                            <span></span>
                        </label> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="database_type">ประเภทฐานข้อมูล</label>
                    <div class="col-sm-9">
                        <select id="database_type" name="database_type" class="form-control m-input " required>
                            <option value="">เลือก</option>
                            <?php
                            if(!empty($database_type)):
                                foreach($database_type as $database):
                                    $selected = '';
                                    if($info->database_type == $database):
                                        $selected = 'selected';
                                    endif;
                            ?>
                            <option value="<?php echo $database;?>" <?=$selected?>><?php echo $database;?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="hostname">Hostname</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->hostname) ? $info->hostname : NULL ?>" type="text" class="form-control m-input " name="hostname" id="hostname" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="username">Username</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->username) ? $info->username : NULL ?>" type="text" class="form-control m-input " name="username" id="username" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="password">Password</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->password) ? $info->password : NULL ?>" type="text" class="form-control m-input " name="password" id="password" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="database">Database</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->database) ? $info->database : NULL ?>" type="text" class="form-control m-input " name="database" id="database" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="dbdriver">DBdriver</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->dbdriver) ? $info->dbdriver : NULL ?>" type="text" class="form-control m-input " name="dbdriver" id="dbdriver" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="port">Port</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->port) ? $info->port : NULL ?>" type="text" class="form-control m-input " name="port" id="port" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="char_set">Char_set</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->char_set) ? $info->char_set : NULL ?>" type="text" class="form-control m-input " name="char_set" id="char_set" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-3 col-form-label" for="dbcollat">DBcollat</label>
                    <div class="col-sm-9">
                        <input value="<?php echo isset($info->dbcollat) ? $info->dbcollat : NULL ?>" type="text" class="form-control m-input " name="dbcollat" id="dbcollat" placeholder="ระบุ" required>
                    </div>
                </div>
            </div>
            
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                            <button type="button" id="btn-test-connect" class="btn btn-primary m-btn--wide" style="float:right;"><i class="fa fa-plug" aria-hidden="true"></i> ทดสอบการเชื่มอต่อ</button>
                        </div>
                    </div>
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="setting_database">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->setting_database_id) ? encode_id($info->setting_database_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>
<div class="col-md-5">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ตัวอย่างการเชื่อมต่อฐานข้อมูล
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="accordion accordion-solid accordion-toggle-plus" id="accordion_example">
                <div class="card">
                    <div class="card-header" id="headingOne6">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse_oracle" aria-expanded="false">
                        <i class="fa fa-database"></i> Setting Database Oracle</div>
                    </div>
                    <div id="collapse_oracle" class="collapse show" data-parent="#accordion_example" style="">
                        <div class="card-body">
                        <?php
                        $oracle['hostname'] = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))(CONNECT_DATA=(SID=TEST01)))';
                        $oracle['username'] =  'SYSTEM';
                        $oracle['password'] =  'password';
                        $oracle['database'] =  'TESTPDB01';
                        $oracle['dbdriver'] =  'oci8';
                        $oracle['port' 	  ] =  1521;
                        $oracle['char_set'] =  'utf8';
                        $oracle['dbcollat'] =  'utf8_general_ci';
                        arr($oracle);
                        ?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo6">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse_mysql" aria-expanded="false">
                        <i class="fa fa-database"></i> Setting Database Mysql</div>
                    </div>
                    <div id="collapse_mysql" class="collapse" data-parent="#accordion_example" style="">
                        <div class="card-body">
                            <?php
                            $mysql['hostname'] = 'localhost';
                            $mysql['username'] =  'root';
                            $mysql['password'] =  'password';
                            $mysql['database'] =  'db_test';
                            $mysql['dbdriver'] =  'mysqli';
                            $mysql['port' 	  ] =  3306;
                            $mysql['char_set'] =  'utf8';
                            $mysql['dbcollat'] =  'utf8_general_ci';
                            arr($mysql);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




