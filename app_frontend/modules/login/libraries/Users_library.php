<?php 

/**
 * Library users
 */
class Users_library 
{
	public $CI;

	public $class;
	public $method;


	public function __construct()
	{
		$this->CI =& get_instance();

		$this->class    = $this->CI->router->fetch_class();
		$this->method   = $this->CI->router->fetch_method();

        $this->CI->load->model('login/users_model');
        $this->CI->load->library('logs_library');
    }

    /**
     * [check_login Check Login]
     * @return redirect
     */
    public function check_login()
    {
        /**
         * [Check cookie]
         */
        $cookie_username = get_cookie('username');
        $cookie_salt = get_cookie('salt');
        if(!$this->_is_logged_in() && $cookie_username && $cookie_salt){

            $this->CI->logs_library->save_log($cookie_username,$this->class,$this->method,"Success : LoginRemember");
            //Login remme
            $this->login_remember($cookie_username,$cookie_salt);
        }
         //END Check cookie
         

        if($this->_is_logged_in()){

          if($this->class === 'login' && $this->method === 'check_login'){
             redirect('dashboard','refresh');
          }	
        }else{
          if($this->class === 'login' && $this->method === 'check_login'){

          }else{
             redirect(base_url('login'),'refresh');
        }
 }

}

    /**
     * [_is_logged_in login is]
     * @return boolean [session] = userUID
     */
    public function _is_logged_in()
    {
        
    	return (bool) $this->CI->session->userdata('users'); 
    }

    /**
     * [login description Logim user]
     * @param  [type]  $username [username]
     * @param  [type]  $password [password]
     * @param  boolean $remember [remember]
     * @return [type]            [$array]
     */
    public function login($username,$password,$remember = FALSE)
    {
        $resp = array();
        if(empty($username) && empty($password)){
            //emoty username and password
            $resp['status'] = 'error';
            $resp['message'] = lang('err_empty');
            return $resp;
        } 

        $users = $this->CI->users_model->find_users_by_user($username);

        if($users){
            if($this->hash_password($password,$users->salt) !== $users->password){
                //Login False
                $resp['status'] = 'warning';
                $resp['message'] = 'username password ไม่ถูกต้อง';
                return $resp;
            }

            //Set session
            $this->_set_session($users);

            //Set Remember
            if($remember){
                $this->_set_remember($users);
            }

            $resp['status'] = 'success';
            $resp['message'] = 'Success : LoginPage';
            return $resp;

        }else{
            //not data
            $resp['status'] = 'warning';
            $resp['message'] = 'ไม่พบ username นี้';
            return $resp;
        }



    }


    /**
     * [salt salt]
     * @return [type] [salt]
     */
    public function salt()
    {
        $raw_salt_len = 16;
        $buffer = '';

        $bl = strlen($buffer);
        for($i = 0; $i < $raw_salt_len; $i++) {
            if($i < $bl) {
                $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0,255));
            }else{
                $buffer .= chr(mt_rand(0,255));
            }
        }

        $salt = $buffer;


        $base64_digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $bcrypt64_digits = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $base64_string = base64_encode($salt);
        $salt = strtr(rtrim($base64_string, '='), $base64_digits, $bcrypt64_digits);

        $salt = substr($salt, 0, 31);

        return $salt;

    }


    /**
     * [hash_password description]
     * @param  [type] $password [password]
     * @param  [type] $salt     [salt]
     * @return [type]           [sha1(password.salt)]
     */
    public function hash_password($password,$salt)
    {
        if(empty($password)){
            return false;
        }

        return sha1($password.$salt);
    }


    private function _set_session($users){
        //Login Success
        $set_session =  array(
            'UID' => $users->user_id,
            'Username' => $users->username,
            'Name' => $users->fname.' '.$users->lname, 
            'RoleId' => $users->role_id,
            'type'=> $users->type,
            'email'=> $users->email
        );

        $this->CI->session->set_userdata('users',$set_session);
    }

    private function _set_remember($users){
        $expire = (60 * 60 * 24 * 365);
        set_cookie([
            'name' => 'username',
            'value' => $users->username,
            'expire' => $expire,
        ]);

        set_cookie([
            'name' => 'salt',
            'value' => $users->salt,
            'expire' => $expire,
        ]);

    }
    

    private function login_remember($users,$salt){

        $users = $this->CI->users_model->find_users_by_user_and_salt($users,$salt);
        if($users){
            //Set Session
            $this->_set_session($users);
            //Set Remember
            $this->_set_remember($users);

            return true;
        }else{
            return false;
        }
    }



}

