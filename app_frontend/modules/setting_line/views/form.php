<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php echo isset($info->active) && $info->active=='1' ? "checked" : NULL ?> type="checkbox" name="active" valus="1"> เปิดใช้งาน
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="token">Token Line</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->token) ? $info->token : NULL ?>" type="text" class="form-control m-input m-input--square" name="token" id="token" required>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="line_url">Line Url</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->line_url) ? $info->line_url : NULL ?>" type="text" class="form-control m-input m-input--square" name="line_url" id="line_url" required>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                            <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="stting_line">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->line_id) ? encode_id($info->line_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div> 
</div> 




