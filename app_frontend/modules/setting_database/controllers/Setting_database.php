<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Setting_database extends MX_Controller 
{

    private $_title             = "ตั้งค่าฐานข้อมูล";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับตั้งค่าฐานข้อมูล";
    private $_grpContent        = "setting_database";
    private $_requiredExport    = true;
    private $_permission;
    private $_database_type     = array('oracle','mysql');

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("setting_database_m");
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        //$action[1][]      = action_filter();
        $action[1][]        = action_add(site_url("{$this->router->class}/create"));
        //$action[2][]      = action_export_group($export);
        $action[3][]        = action_trash_multi("{$this->router->class}/destroy");
        $action[3][]        = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $info               = $this->setting_database_m->get_rows($input);
        $infoCount          = $this->setting_database_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                             = encode_id($rs->setting_database_id);
            $action                         = array();
            $action[1][]                    = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active                         = $rs->active ? "checked" : null;

            $is_default = '';
            if($rs->is_default > 0):
                $is_default = '<p><small class="m-badge m-badge--success m-badge--wide">(ตั้งเป็นค่าเริ่มต้น)</small></p>';
            endif;
            $column[$key]['DT_RowId']       = $id;
            $column[$key]['checkbox']       = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['hostname']       = $rs->hostname;
            $column[$key]['database_type']  = $rs->database_type.$is_default;
            $column[$key]['database']       = $rs->database;
            $column[$key]['dbdriver']       = $rs->dbdriver;
            $column[$key]['active']         = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at']     = datetime_table($rs->created_at);
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['action']         = Modules::run('utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));
        $data['database_type']  = $this->_database_type;
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->setting_database_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                 = decode_id($id);
        $input['setting_database_id']    = $id;
        $info               = $this->setting_database_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        $data['database_type']  = $this->_database_type;
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->setting_database_m->update($id, $value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['database_type'] = $input['database_type'];
        $value['dsn']           = $input['dsn'];
        $value['hostname']      = $input['hostname'];
        $value['username']      = $input['username'];
        $value['password']      = $input['password'];
        $value['database']      = $input['database'];
        $value['dbdriver']      = $input['dbdriver'];
        $value['port']          = $input['port'];
        $value['char_set']      = $input['char_set'];
        $value['dbcollat']      = $input['dbcollat'];
        $value['is_default']    = !empty($input['is_default']) ? 1 : 0;

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->setting_database_m->get_rows($input);
        $fileName       = "repo";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->setting_database_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "repo.pdf";
        $pathFile = "uploads/pdf/repo/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->setting_database_m->get_rows($input);
        $infoCount          = $this->setting_database_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->setting_database_id);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->setting_database_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->setting_database_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->setting_database_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result                 = false;

                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->setting_database_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

                $data['success']    = $result;
                $data['toastr']     = $toastr;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'content';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function api_test_connect()
    {

        if ( !$this->_permission ):
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        else:
            $input = $this->input->post();
           
            $config['dsn'	  ] =  '';
            $config['hostname'] = $input['hostname'];
            $config['username'] = $input['username'];
            $config['password'] = $input['password'];
            $config['database'] = $input['database'];
            $config['dbdriver'] = $input['dbdriver'];
            $config['port' 	  ] = $input['port'];
            $config['dbprefix'] =  '';
            $config['pconnect'] =  FALSE;
            $config['db_debug'] =  (ENVIRONMENT !== 'production');
            $config['cache_on'] =  FALSE;
            $config['cachedir'] =  'uploads/cache/';
            $config['char_set'] =  !empty($input['char_set']) ? $input['char_set'] : 'utf8';
            $config['dbcollat'] =  !empty($input['dbcollat']) ? $input['dbcollat'] : 'utf8_general_ci';
            $config['swap_pre'] =  '';
            $config['encrypt']  =  FALSE;
            $config['compress'] =  FALSE;
            $config['stricton'] =  FALSE;
            $config['failover'] =  array();
            $config['save_queries'] =  TRUE; 

            $result = $this->load->database($config, true);
                
            if ( $result ):
                $result             = true;
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'สามารถเชื่อมต่อฐานข้อมูลได้';
            else:
                $result             = false;
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'ไม่สามารถเชื่อมต่อฐานข้อมูลได้ กรุณาลองใหม่อีกครั้ง...';
            endif;

            $data['success']    = $result;
            $data['toastr']     = $toastr;
            
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
        endif;
        
    }
    
}
