<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'third_party/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Oracles extends MX_Controller
{

    private $_title             = "Oracles";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับOracles";
    private $_grpContent        = "oracles";
    private $_requiredExport    = true;
    private $_permission;
    private $_dbConnect;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if (!$this->_permission && !$this->input->is_ajax_request()) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("oracles_m");
        $this->load->model("setting_database/setting_database_m");
    }

    private function set_setting_database($database = null)
    {

        if (!empty($database)) :
            $config['dsn']     = '';
            $config['hostname']     = !empty($database->hostname) ? $database->hostname : 'localhost';
            $config['username']     = !empty($database->username) ? $database->username : '';
            $config['password']     = !empty($database->password) ? $database->password : '';
            $config['database']     = !empty($database->database) ? $database->database : '';
            $config['dbdriver']     = !empty($database->dbdriver) ? $database->dbdriver : '';
            $config['port']     = !empty($database->port) ? $database->port : 3306;
            $config['dbprefix']     = '';
            $config['pconnect']     = FALSE;
            $config['db_debug']     = (ENVIRONMENT !== 'production');
            $config['cache_on']     = FALSE;
            $config['cachedir']     = 'uploads/cache/';
            $config['char_set']     = !empty($database->char_set) ? $database->char_set : 'utf8';
            $config['dbcollat']     = !empty($database->dbcollat) ? $database->dbcollat : 'utf8_general_ci';
            $config['swap_pre']     = '';
            $config['encrypt']      = FALSE;
            $config['compress']     = FALSE;
            $config['stricton']     = FALSE;
            $config['failover']     = array();
            $config['save_queries'] = TRUE;
            $this->_dbConnect = $this->load->database($config, true);
        endif;
    }

    public function index()
    {
        $this->load->module('template');

        $action[1][]        = action_refresh(site_url("{$this->router->class}"));;
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);

        $input = $this->input->post();

        $input_['recycle']          = 0;
        $input_['active']           = 1;
        $input_['is_default']       = 1;
        $input_['database_type']    = 'mysql';
        $setting_database           = $this->setting_database_m->get_rows($input_)->result();
        $data['setting_database']   = $setting_database;

        if (!empty($input['setting_database_id'])) :
            $input__['setting_database_id']    = $input['setting_database_id'];
            $setting_database           = $this->setting_database_m->get_rows($input__)->row();
            $this->set_setting_database($setting_database);
        else :
            $this->set_setting_database($setting_database[0]);
        endif;

        ini_set('memory_limit', '-1');

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['frmAction']      = site_url("{$this->router->class}/index");
        $tables                 = $this->_dbConnect->list_tables();
        $data['mysqltables']    = $tables;

        $selecttables            = $this->input->post('mysqltables');
        $textarea_select_tables  = $this->input->post('textarea_select_tables');
        if (!empty($textarea_select_tables)) :

            $info = $this->_dbConnect->query($textarea_select_tables);
            if (!empty($info)) :
                $data['table_thead']            = $info->list_fields();
                $data['table_result']           = $info->result();
                $data['textarea_select_tables'] = $textarea_select_tables;
                $data['selecttables']           = $selecttables;
                $data['last_query']             = $this->_dbConnect->last_query();
                $data['setting_database_id']    = $input['setting_database_id'];
            else :
                $data['textarea_select_tables'] = $textarea_select_tables;
                $data['setting_database_id']    = $input['setting_database_id'];
            endif;
        else :
            $data['textarea_select_tables'] = $textarea_select_tables;
            $data['setting_database_id']    = $input['setting_database_id'];
        endif;

        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";

        $this->template->layout($data);
    }

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $info               = $this->mysqls_m->get_rows($input);
        $infoCount          = $this->mysqls_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ($this->_requiredExport) {
            $condition[$this->_grpContent] = $input;
            $this->session->set_userdata("condition", $condition);
        }

        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->repoId);
            $action                     = array();
            $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['active']     = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action']     = Modules::run('utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function database_lists()
    {
        $input              = $this->input->post();
        $data               = array();
        $data['success']    = false;
        if (!empty($input['setting_database_id'])) :
            ini_set('memory_limit', '-1');
            $input__['setting_database_id']    = $input['setting_database_id'];
            $setting_database           = $this->setting_database_m->get_rows($input__)->row();
            $this->set_setting_database($setting_database);
            $tables                     = $this->_dbConnect->list_tables();
            if (!empty($tables)) :
                $data['mysqltables']        = $tables;
                $data['success']            = true;
            else :
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                $data['toastr']     = $toastr;
            endif;
        else :
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            $data['toastr']     = $toastr;
        endif;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function export()
    {
        $this->load->module('template');
        $data['frmActionCSV']      = site_url("{$this->router->class}/excel");
        $data['frmActionPDF']      = site_url("{$this->router->class}/pdf");
        $input                     = $this->input->post();
        ini_set('memory_limit', '-1');
        if (!empty($input['setting_database_id'])) :
            $input__['setting_database_id']    = $input['setting_database_id'];
            $setting_database           = $this->setting_database_m->get_rows($input__)->row();
            $data['setting_database']   = $setting_database;
            $this->set_setting_database($setting_database);
        endif;

        $textarea_select_tables = $this->input->post('query_select_tables');

        if (!empty($textarea_select_tables)) :
            $info = $this->_dbConnect->query($textarea_select_tables);
            if (!empty($info)) :
                $data['table_thead']            = $info->list_fields();
                $data['table_result']           = $info->result();
                $data['textarea_select_tables'] = $textarea_select_tables;
            endif;
        endif;

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('นำออก', site_url("{$this->router->class}/create"));
        // page detail
        $data['pageHeader']     = 'นำออก';
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/export";
        $data['pageScript']     = "scripts/backend/mysqls/export.js";

        $this->template->layout($data);
    }

    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));

        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->mysqls_m->insert($value);
        if ($result) {
            Modules::run('utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }

    public function edit($id = "")
    {
        $this->load->module('template');

        $id                 = decode_id($id);
        $input['repoId']    = $id;
        $info               = $this->mysqls_m->get_rows($input);
        if ($info->num_rows() == 0) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));

        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->mysqls_m->update($id, $value);
        if ($result) {
            Modules::run('utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }

    private function _build_data($input)
    {

        $value['title']     = $input['title'];
        $value['excerpt']   = $input['excerpt'];
        $value['detail']    = $input['detail'];
        if ($input['mode'] == 'create') {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function excel()
    {
        set_time_limit(0);

        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ($this->session->condition[$this->_grpContent]) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }

        ini_set('memory_limit', '-1');

        $input  = $this->input->post();
        if (!empty($input['setting_database_id'])) :
            $input__['setting_database_id']    = $input['setting_database_id'];
            $setting_database           = $this->setting_database_m->get_rows($input__)->row();
            $data['setting_database']   = $setting_database;
            $this->set_setting_database($setting_database);
        endif;

        $textarea_select_tables = $this->input->post('query_select_tables');

        if (!empty($textarea_select_tables)) :
            $info = $this->_dbConnect->query($textarea_select_tables);
            if (!empty($info)) :
                $header            = $info->list_fields();
                $table_result      = $info->result_array();

                $export_name       = $this->input->post('export_name');
                if (!empty($export_name)) :
                    $filename = $export_name . '.csv';
                else :
                    $filename = 'export_' . date('Ymd') . '.csv';
                endif;

                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=$filename");
                header("Content-Type: application/csv; ");

                // file creation 
                $file = fopen('php://output', 'w');


                fputcsv($file, $header);
                foreach ($table_result as $key => $line) {
                    fputcsv($file, $line);
                }
                fclose($file);
                exit;
            endif;
        endif;
    }

    public function excels()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');

        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ($this->session->condition[$this->_grpContent]) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->mysqls_m->get_rows($input);
        $fileName       = "mysqls";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";

        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();

        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data' => 'รายการ', 'width' => 50),
            'B' => array('data' => 'เนื้อหาย่อ', 'width' => 50),
            'C' => array('data' => 'วันที่สร้าง', 'width' => 16),
            'D' => array('data' => 'วันที่แก้ไข', 'width' => 16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);

        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: ' . date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);

        //header
        $rowCount = 2;
        foreach ($colums as $colum => $data) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum . $rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }

        // data
        $rowCount++;
        foreach ($info->result() as $row) {
            foreach ($fields as $key => $field) {
                $value = $row->{$field};
                if ($field == 'created_at' || $field == 'updated_at')
                    $value = datetime_table($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value);
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');
        exit;
    }

    public function pdf()
    {
        set_time_limit(0);

        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ($this->session->condition[$this->_grpContent]) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }

        ini_set('memory_limit', '-1');
        $input  = $this->input->post();
        if (!empty($input['setting_database_id'])) :
            $input__['setting_database_id']    = $input['setting_database_id'];
            $setting_database           = $this->setting_database_m->get_rows($input__)->row();
            $data['setting_database']   = $setting_database;
            $this->set_setting_database($setting_database);
        endif;

        $textarea_select_tables = $this->input->post('query_select_tables');

        if (!empty($textarea_select_tables)) :
            $info = $this->_dbConnect->query($textarea_select_tables);
            if (!empty($info)) :
                $data['table_thead']            = $info->list_fields();
                $data['table_result']           = $info->result();
            endif;
        endif;

        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];

        $export_name       = $this->input->post('export_name');
        if (!empty($export_name)) :
            $fileName = $export_name . '.pdf';
        else :
            $fileName = 'export_' . date('Ymd') . '.pdf';

        endif;

        $data['fileName'] = $fileName;

        require APPPATH . '../vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;

        $pathFile = "uploads/pdf/mysqls/";
        if ($saveFile) {
            create_dir($pathFile);
            $fileName = $pathFile . $fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ($download)
            $mpdf->Output($fileName, 'D');
        if ($viewFile)
            $mpdf->Output();
    }

    public function trash()
    {
        $this->load->module('template');

        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";

        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->mysqls_m->get_rows($input);
        $infoCount          = $this->mysqls_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->repoId);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function destroy()
    {
        if (!$this->_permission) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ($input['id'] as &$rs)
                $rs                     = decode_id($rs);
            $dateTime               = db_datetime_now();
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];

            $result                 = false;
            $value['active']        = 0;
            $value['recycle']       = 1;
            $value['recycle_at']    = $dateTime;
            $value['recycle_by']    = $this->session->users['user_id'];
            $result                 = $this->mysqls_m->update_in($input['id'], $value);

            if ($result) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function restore()
    {
        if (!$this->_permission) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs)
                $rs                     = decode_id($rs);
            $dateTime               = db_datetime_now();
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];

            $result                 = false;
            $value['active']        = 0;
            $value['recycle']       = 0;
            $result                 = $this->mysqls_m->update_in($input['id'], $value);

            if ($result) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function delete()
    {
        if (!$this->_permission) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs)
                $rs                     = decode_id($rs);
            $dateTime               = db_datetime_now();
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];

            $result                 = false;
            $value['active']        = 0;
            $value['recycle']       = 2;
            $result                 = $this->mysqls_m->update_in($input['id'], $value);

            if ($result) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function action($type = "")
    {
        if (!$this->_permission) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs)
                $rs                     = decode_id($rs);
            $dateTime               = db_datetime_now();
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];
            $result                 = false;

            if ($type == "active") {
                $value['active']    = $input['status'] == "true" ? 1 : 0;
                $result             = $this->mysqls_m->update_in($input['id'], $value);
            }

            if ($result) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function sumernote_img_upload()
    {
        //sumernote/img-upload
        $path       = 'content';
        $upload     = $this->uploadfile_library->do_upload('file', TRUE, $path);

        if (isset($upload['index'])) {
            $picture = $this->config->item('root_url') . 'uploads/' . $path . '/' . date('Y') . '/' . date('m') . '/' . $upload['index']['file_name'];
        }

        echo $picture;
    }

    public function deletefile($ids)
    {

        $arrayName = array('file' => $ids);

        echo json_encode($arrayName);
    }

    public function testOracle()
    {
        $config['dsn'] =  '';
        $config['hostname'] = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))(CONNECT_DATA=(SID=TEST01)))';
        // $config['hostname'] =  'SYSTEM@//localhost:1521/xe';
        $config['username'] =  'SYSTEM';
        $config['password'] =  'password';
        $config['database'] =  'TESTPDB01';
        $config['dbdriver'] =  'oci8';
        // $config['port' 	  ] =  3306;
        $config['dbprefix'] =  '';
        $config['pconnect'] =  FALSE;
        $config['db_debug'] =  (ENVIRONMENT !== 'production');
        $config['cache_on'] =  FALSE;
        $config['cachedir'] =  'uploads/cache/';
        $config['char_set'] =  'utf8';
        $config['dbcollat'] =  'utf8_general_ci';
        $config['swap_pre'] =  '';
        $config['encrypt'] =  FALSE;
        $config['compress'] =  FALSE;
        $config['stricton'] =  FALSE;
        $config['failover'] =  array();
        $config['save_queries'] =  TRUE;

        $this->load->database($config, true);
    }
}
