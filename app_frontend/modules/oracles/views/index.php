<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'id' => 'frm-submit-data', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label text-right" for="">Database</label>
                <div class="col-sm-3">
                    <?php
                    if (!empty($setting_database)) :
                        if (count($setting_database) > 0) :
                    ?>
                            <select id="select-database-list" class="form-control m-input select2" name="setting_database_id">
                                <?php
                                foreach ($setting_database as $setting) :
                                    $selected  = '';
                                    if ($setting->setting_database_id == $setting_database_id) :
                                        $selected = 'selected';
                                    endif;
                                ?>
                                    <option value="<?php echo !empty($setting->setting_database_id) ? $setting->setting_database_id : ''; ?>" <?= $selected ?>><?php echo !empty($setting->database) ? $setting->database : ''; ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        <?php
                        else :
                        ?>
                            <input value="<?php echo !empty($setting_database[0]->database) ? $setting_database[0]->database : ''; ?>" type="text" class="form-control m-input " name="setting_database_id" id="" placeholder="ระบุ" required readonly>
                    <?php
                        endif;
                    endif;
                    ?>

                </div>
                <label class="col-sm-1 col-form-label text-right" for="">Table List</label>
                <div class="col-sm-3">
                    <select id="select-tables-list" class="form-control m-input select2" name="mysqltables">
                        <option value="">เลือกตาราง</option>
                        <?php
                        if (!empty($mysqltables)) :
                            foreach ($mysqltables as $tables) :
                                $select  = '';
                                if ($tables == $selecttables) :
                                    $select = 'selected';
                                endif;
                        ?>
                                <option value="<?php echo !empty($tables) ? $tables : ''; ?>" <?= $select ?>><?php echo !empty($tables) ? $tables : ''; ?></option>
                        <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label text-right" for="">SQL Command</label>
                <div class="col-sm-7">
                    <textarea id="textarea-select-tables" class="form-control m-input" name="textarea_select_tables" rows="10" col="5" placeholder="ระบุ"><?php echo !empty($textarea_select_tables) ? $textarea_select_tables : "select * from xxxxx"; ?></textarea>
                </div>
                <div class="col-sm-2">
                    <button type="submit" id="btn-query-sql" class="btn btn-success m-btn--wide" style="width: 100%;margin-bottom: 10px;"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</button>
                    <a href="javascript:void(0)" id="btn-query-sql-export" class="btn btn-info m-btn--wide" style="width: 100%;margin-bottom: 10px;"><i class="fa fa-download" aria-hidden="true"></i> Export</a> 
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label text-right" for="">SQL Query</label>
                <div class="col-sm-10">
                    <div class="alert m-alert--default"> <?php echo !empty($last_query) ? $last_query : 'Not Query String'; ?></div>
                </div>
            </div>

            <br>
            <div class="table-responsive">
                <table id="data-lists" class="table table-hover dataTable" width="100%">
                    <thead>
                        <?php
                        if (!empty($table_thead)) :
                        ?>
                            <tr>
                                <?php
                                foreach ($table_thead as $thead) :
                                ?>
                                    <th style="text-align: left;"><?php echo !empty($thead) ? $thead : ''; ?></th>
                                <?php
                                endforeach;
                                ?>
                            </tr>
                        <?php
                        else :
                        ?>
                            <th style="text-align: left;">FieLd 1</th>
                            <th style="text-align: left;">FieLd 2</th>
                            <th style="text-align: left;">FieLd 3</th>
                            <th style="text-align: left;">FieLd 4</th>
                            <th style="text-align: left;">FieLd 5</th>
                            <th style="text-align: left;">FieLd 6</th>
                            <th style="text-align: left;">FieLd 7</th>
                        <?php
                        endif;
                        ?>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($table_thead)) :
                            if (!empty($table_result)) :
                                foreach ($table_result as $result) :
                        ?>
                                    <tr>
                                        <?php
                                        foreach ($table_thead as $thead) :
                                            $thead = !empty($thead) ? $thead : "";
                                        ?>
                                            <td><?php echo !empty($result->$thead) ? $result->$thead : ''; ?></td>
                                        <?php
                                        endforeach;
                                        ?>
                                    </tr>
                            <?php
                                endforeach;
                            endif;
                        else :
                            ?>
                            <tr>
                                <td colspan="7" class="text-center">Not Data</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>
<?php echo form_open_multipart(base_url('oracles/export'), array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create-query-export', 'method' => 'post', 'autocomplete' => 'off')) ?>
<textarea id="query_select_tables" class="form-control m-input" name="query_select_tables" rows="10" col="5" placeholder="ระบุ" hidden><?php echo !empty($last_query) ? $last_query : ''; ?></textarea>
<input type="hidden" name="setting_database_id" value="<?php echo !empty($setting_database_id) ? $setting_database_id : ''; ?>">
<?php echo form_close() ?>