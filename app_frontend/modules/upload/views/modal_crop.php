<div class="modal fade" id="modal-crop">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">แก้ไขภาพ</h4>
            </div>
            <div class="modal-body">
                <div class="img-container" >
                    <img id="image-crop" src="<?php echo base_url('assets/images/no_image.png') ?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="modal-no" class="btn btn-flat btn-default" ><i class="fa fa-times"></i> ยกเลิก</button>
                <button type="button" id="modal-yes" class="btn btn-flat btn-primary"><i class="fa fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
<style>
.img-container {
  max-width: 100%;
  max-height: 480px;
}
</style>
