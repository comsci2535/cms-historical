<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{
	private $_title = 'ผู้ใช้งาน';
    private $_pageExcerpt = 'การจัดการเกี่ยวกับผู้ใช้งาน';
    private $_permission;
    private $_grpContent = 'users';

	public function __construct()
	{
		parent::__construct();
		$this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
		$this->load->library('users_library');
		$this->load->library('logs_library');
		$this->load->library('encryption');
		$this->load->model("users_m");
	}

	public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       // $action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->users_m->get_rows($input);
        $infoCount = $this->users_m->get_count($input);
        $column = array();
       
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->user_id);
            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
           
            if($rs->username == "super_admin"){
              $column[$key]['checkbox'] = "<i class='fa fa-lock'></i>";
            }else{
              $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            }
            $column[$key]['username'] = $rs->username;
            $column[$key]['title'] = $rs->fullname;
            $column[$key]['excerpt'] = $rs->email;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create(){
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/save");
        $data['roles'] = Modules::run('roles/dropdown');
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function save(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        //arr($value);exit();
        $result = $this->users_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id=""){
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['user_id'] = $id;
        $info = $this->users_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");
        $data['frmActionPassword'] = site_url("{$this->router->class}/update_password");
        $data['roles'] = Modules::run('roles/dropdown');
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/edit";
        
        $this->template->layout($data);
    }
    
    public function update(){
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->users_m->update($id, $value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
     public function update_password() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['id']);
        
        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
        $value['password'] = $password;
        $value['salt'] = $salt;
        $result = $this->users_m->update($input['user_id'], $value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    
    
    private function _build_data($input){

        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        
        $value['role_id'] = $input['role_id'];
        $value['email'] = $input['email'];
        $value['fname'] = $input['fname'];
        $value['lname'] = $input['lname'];
        $value['phone'] = $input['phone'];
      
        $value['fullname'] = $input['fname'].' '.$input['lname'];
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['type'] = "admin";
            $value['password'] = $password;
            $value['salt'] = $salt;
            $value['username'] = $input['username'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function trash(){
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash(){
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->users_m->get_rows($input);
        $infoCount = $this->users_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->user_id);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->username;
            $column[$key]['excerpt'] = $rs->fullname;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->users_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycle_at'] = $dateTime;
                $value['recycle_by'] = $this->session->users['user_id'];
                $result = $this->users_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->users_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->users_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    public function check_username()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        $info = $this->users_m->get_rows_check($input);
//       arrx($info);
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->user_id == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }
	
	public function signin()
	{
		$this->load->view('sign-in');
	}

	public function login()
    {
    	//arr('1');exit();

		/**
         * test password
         */
		/*$salt = $this->users_library->salt();
		$password = $this->users_library->hash_password('password', $salt);
		echo 'Pass : '.$password;
		echo 'salt : '.$salt ;
		
		$salt = 'd833837a9fd53585ec234e61a901bc71a1a4abd9';
		$password = 'FoyL3gEokVEMRwyWFTJ5xO4422829e0de131db9c96e93c5bb5bfe875a9790e';

		if($this->users_library->hash_password('password',$salt) !== $password){
			echo '<br>'.'NOT';
		}else {
			echo '<br>'.'YES';
		}*/

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$remember = (bool)$this->input->post('remember');

			$users = $this->users_library->login(
				$username,
				$password,
				$remember
			);
  			
            
            //Log
		    //$this->logs_library->save_log($username,$this->class,$this->method,$users['message']);

			if($users['status'] === 'success'){
				$users = $this->session->users;
                $users['isBackend'] = TRUE;
                $users['user_id'] = $users['UID'];
                $users['image'] = $this->config->item('template')."assets/app/media/img/users/100_3.jpg";
                $this->session->set_userdata('users', $users);
                $this->session->set_flashdata('firstTime', '1');
                //$this->login_m->update_last_login();
				
				//Go to  dashboard
				redirect(base_url('dashboard'),'refresh');
			}elseif ($users['status'] === 'warning') {
			
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('users/login'),'refresh');
			}else{
				
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('users/login'),'refresh');
			}

			
		}
		else
		{
            //loade view
			$this->load->view('sign-in');
		}

	}

	public function Logout()
	{
		$usename = $this->session->userdata('users');
		//Log
		//$this->logs_library->save_log($usename['Username'],$this->class,$this->method,'Logout : Logouted');

		delete_cookie('username');
		delete_cookie('salt');

		$this->session->unset_userdata('users');
		redirect(base_url('users/login'),'refresh');
	}
}
