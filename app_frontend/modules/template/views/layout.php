<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?=$this->config->item('template'); ?>assets/demo/default/media/img/logo/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=$this->config->item('template'); ?>assets/demo/default/media/img/logo/favicon.ico" type="image/x-icon">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title> 
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Sarabun:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>




    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
     <?php if (in_array($this->router->method, array('create','edit','manage','index'))) : ?> 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
     <?php endif; ?>
    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="<?=$this->config->item('template'); ?>assets/demo/default/media/img/logo/favicon.ico" />

    <!--end::Page Vendors Styles -->
    <link href="<?=$this->config->item('template'); ?>assets/vendors/custom/summernote-bs4/summernote.css" rel="stylesheet" />

    <!-- fileinput -->
    <link href="<?=$this->config->item('template'); ?>assets/vendors/custom/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('template'); ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <!-- /////////////////////////////////////// -->

    <!--end::Web font -->
    <link href="<?=$this->config->item('template'); ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />


    <!--begin::Page Vendors Styles -->
    <link href="<?=$this->config->item('template'); ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />




    <!--css tari-->
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css">
    <link href="<?=$this->config->item('template'); ?>assets/plugins/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
    <link href="<?=$this->config->item('template'); ?>assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css"/>
    <?php if (in_array($this->router->method, array('order'))) : ?>  
        <link href="<?=$this->config->item('template'); ?>assets/plugins/nestable2/jquery.nestable.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <!--css tari-->

    <link href="<?=$this->config->item('template'); ?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

     <style type="text/css">
        label.error,.error-dup ,#title-error{
            color: red;
        }
         .help-block{
            color: red;
        }

    </style>

</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <?php $this->load->view('header'); ?>
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <?php $this->load->view('sidebar'); ?>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <?php $this->load->view('content_header'); ?>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--Begin::Section-->
                    <div class="row">
                        <?php $this->load->view($contentView); ?>
                    </div>
                </div>
            </div> 

        </div>
        <!-- begin::Footer -->
        <footer class="m-grid__item     m-footer ">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                        <span class="m-footer__copyright">
                            2020 &copy; Backend by <a href="#" class="m-link">Admin Historical</a>
                        </span>
                    </div>
                <!-- <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                    <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <span class="m-nav__link-text">About</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <span class="m-nav__link-text">Privacy</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <span class="m-nav__link-text">T&C</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <span class="m-nav__link-text">Purchase</span>
                            </a>
                        </li>
                        <li class="m-nav__item m-nav__item">
                            <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                                <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div> -->
            </div>
        </div>
    </footer>
</div>
<?php $this->load->view('asside'); ?>

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!-- begin::Quick Nav -->

<!--begin::Global Theme Bundle -->
<script src="<?=$this->config->item('template'); ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--begin:: bootstrap-datetimepicker -->
<script src="<?=$this->config->item('template'); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!--end:: bootstrap-datetimepicker -->

<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!-- summernote -->
<script type="text/javascript" src="<?=$this->config->item('template'); ?>assets/vendors/custom/summernote-bs4/summernote.js"></script>

<!-- slug -->
<script type="text/javascript" src="<?=$this->config->item('template'); ?>assets/vendors/custom/slug/slug.js"></script>

<!-- fileinput -->
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/bootstrap-fileinput-master/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/bootstrap-fileinput-master/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/bootstrap-fileinput-master/js/locales/th.js"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/custom/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>

<!--tari Scripts -->
<script src="<?php echo $this->config->item('template') ?>assets/plugins/bootbox/bootbox.min.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/jquery.form.min.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/jquery-validation/dist/localization/messages_th.js"></script>  
<script src="<?php echo $this->config->item('template') ?>assets/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>

<?php if (in_array($this->router->class, array('report_website','dashboard'))) : ?> 
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/highcharts.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/highcharts-3d.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/modules/exporting.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/modules/export-data.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/modules/drilldown.js"></script>
<script src="<?php echo $this->config->item('template') ?>assets/plugins/highcharts/modules/map.js" type="text/javascript"></script>
<?php endif; ?>

<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/jquery.Thailand.min.css">
<script type="text/javascript" src="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/jquery.Thailand.min.js"></script>

<script src="<?=$this->config->item('template'); ?>assets/vendors/vendors/jquery-migrate3.0.1/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('template'); ?>/assets/demo/default/custom/components/base/blockui.js" type="text/javascript"></script>
<!-- <script src="<?php echo $this->config->item('template') ?>assets/demo/default/custom/crud/datatables/data-sources/html.js" type="text/javascript"></script> -->
<?php if (in_array($this->router->class, array('courses'))) : ?> 
<script src="https://player.vimeo.com/api/player.js"></script>
<?php endif; ?>
<?php if (in_array($this->router->method, array('order'))) : ?>  
    <script src="<?php echo $this->config->item('template') ?>assets/plugins/nestable2/jquery.nestable.js" type="text/javascript"></script>
<?php endif; ?>
<script type="text/javascript">
    var appName = "<?=config_item('appName');?>";
    var csrfToken = get_cookie('csrfCookie');
    var siteUrl = "<?php echo $this->config->item('root_url'); ?>";
    var baseUrl = "<?php echo $this->config->item('root_url'); ?>";
    var controller = "<?php echo $this->router->class ?>";
    var method = "<?php echo $this->router->method ?>";
    function get_cookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
</script>
<script src="<?php echo $this->config->item('scripts') ?>app.js?v=<?php echo rand(0,100) ?>"></script>

<?php  echo $pageScript; ?>

<script>
$(document).ready(function () {
    <?php if ($this->session->toastr) : ?>
        setTimeout(function () {
            toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
        }, 500);
        <?php $this->session->unset_userdata('toastr'); ?>
    <?php endif; ?>
});

check_dup_slug = true;
check_dup_name = true;
slugURL();

//load summernote
EditorTool();

$('#data-list-dashboard').dataTable({
    searching:false
});

$('.select2').select2();
//Skin changer
function slugURL() {

    var typingTimer;                
    var doneTypingInterval = 300;  
    var $slug = $('#slug');
    var $name = $('#title');
    var $id = $('#id');

    $slug.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
        $('#slug').val(slug($slug.val()));
        $('#slug').parent().addClass('focused');
    });

    $slug.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    $name.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTypingName, doneTypingInterval);
        stypingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
        $('#slug').val(slug($name.val()));
        $('#slug').parent().addClass('focused');
    });

    $name.on('keydown', function () {
        clearTimeout(typingTimer);
    });
} 

function doneTypingSlug () {
    $.ajax({
        url: '<?=base_url()."slugs/check_slugs"?>',
        type: 'POST',
        dataType: 'json',
        data: {name: $('#slug').val(), id: $('#id').val() , db: $('#db').val(), csrfToken : get_cookie('csrfCookie')},
    })
    .done(function(data){
        setTimeout(function(){

            if(data){
                $('#slug').parent().removeClass('error');  
                $('#slug-error-dup').hide()
                check_dup_slug=true;

            }else{
                $('#slug-error-dup').show()
                $('#slug').parent().addClass('error'); 
                check_dup_slug=false;
            }
        }, 300);    
    })
    .fail(function(e) {
        console.log(e);
    })
    .always(function(c) {
        console.log(c);
    });
}  

function doneTypingName ($name,$id = null) {
    $.ajax({
        url: '<?=base_url()."slugs/check_name"?>',
        type: 'POST',
        dataType: 'json',
        data: {name: $('#title').val(), id: $('#id').val() , db: $('#db').val() , csrfToken : get_cookie('csrfCookie')},
    })
    .done(function(data){
        setTimeout(function(){

            if(data){
                $('#title').parent().removeClass('error');  
                $('#name-error-dup').hide()
                check_dup_slug=true;

            }else{
                $('#name-error-dup').show()
                $('#title').parent().addClass('error'); 
                check_dup_slug=false;
            }
        }, 300);    
    })
    .fail(function(e) {
        console.log(e);
    })
    .always(function(c) {
        console.log(c);
    });
} 

//summernote
function EditorTool(){
    $('.summernote').summernote({
        fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 300,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile(files[0]);
            }
        },

    });
} 

function sendFile(file, editor, welEditable) {

    data = new FormData();
    data.append("file", file);
    data.append("csrfToken", get_cookie('csrfCookie'));
    $.ajax({
        data: data,
        type: 'POST',
        url: "<?=base_url()?>"+controller+"/sumernote_img_upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
            $('.summernote').summernote('editor.insertImage', data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
        }
    });
} 

</script>

</body>
</html>
