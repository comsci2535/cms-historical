<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete'=>'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">กลุ่มผู้ใช้งาน</label>
                    <div class="col-sm-7">
                        <?php echo form_dropdown('role_id', $roles, NULL, 'class="form-control select2" required') ?>
                    </div>
                </div>     
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >อีเมล์</label>
                    <div class="col-sm-7">
                        <input value="" type="email" id="input-email" class="form-control" name="email" required >
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อ</label>
                    <div class="col-sm-7">
                        <input value="" type="text" id="input-username" class="form-control" name="fname" required>
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >นามสกุล</label>
                    <div class="col-sm-7">
                        <input value="" type="text" id="input-username" class="form-control" name="lname">
                    </div>
                </div> 
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >เบอร์โทรศัพท์</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->phone ?>" type="text" id="input-phone" class="form-control" name="phone">
                    </div>
                </div> 
               <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อผู้ใช้งาน</label>
                    <div class="col-sm-7">
                        <input value="" type="text" id="input-username" class="form-control" name="username">
                    </div>
                </div>  
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">รหัสผ่าน</label>
                    <div class="col-sm-7">
                        <input value="" type="password" id="input-password" class="form-control" name="password" required>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ยืนยันรหัสผ่าน</label>
                    <div class="col-sm-7">
                        <input value="" type="password" id="input-repassword" class="form-control" name="rePassword">
                    </div>
                </div>   
                 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                            <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">ยกเลิก</button>
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="repo">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->user_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->user_id;?>';

</script>




