
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <form  role="form">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label text-right" for="">Path File</label>
                    <div class="col-sm-3">
                        <input value="Softnix" type="text" class="form-control m-input " name="" id="" placeholder="ระบุ" required> 
                    </div> 
                </div>
                <br>
            
                <table id="data-lists" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: left;">วันที่สร้าง</th>
                            <th style="text-align: left;">ชื่อไฟล์</th>
                            <th style="text-align: left;">ขนาดไฟล์</th>
                            <th style="text-align: left;">ดาวน์โหลด</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($info)):
                            foreach($info as $item):
                        ?>
                        <tr>
                            <td><?php echo !empty($item['date_full'])? $item['date_full'] : '';?></td>
                            <td><?php echo !empty($item['name'])? $item['name'] : '';?></td>
                            <td><?php echo !empty($item['size_full'])? $item['size_full'] : '';?></td>
                            <td><a href="<?php echo !empty($item['path']) ? base_url($item['path']) : "javascript:void(0)"?>" download="<?php echo !empty($item['name'])? $item['name'] : '';?>"><?php echo !empty($item['name'])? $item['name'] : '';?></a></td>
                        </tr>
                         <?php
                            endforeach;
                         endif;
                         ?>
                    </tbody>
                </table>
            </form>

        </div>
    </div>
</div>