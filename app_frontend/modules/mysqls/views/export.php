<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <?php echo form_open_multipart($frmActionCSV, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create form-submit-expert-file', 'method' => 'post', 'autocomplete' => 'off', 'target' => '_blank')) ?>
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group" role="group" aria-label="First group">
                    <button type="button" class="btn btn-sm btn-success m-btn--wide btn-export-file-query" data-form="<?= $frmActionCSV ?>"><i class="fa fa-file-excel" aria-hidden="true"></i> CSV</button>
                    <button type="button" class="btn btn-sm btn-info m-btn--wide btn-export-file-query" data-form="<?= $frmActionPDF ?>"><i class="fa fa-file" aria-hidden="true"></i> PDF</button>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label text-right" for="">Database</label>
                <div class="col-sm-3">
                    <input value="<?php echo !empty($setting_database->database) ? $setting_database->database : '' ?>" type="text" class="form-control m-input " name="" placeholder="ระบุ" required readonly>
                    <input type="hidden" name="setting_database_id" value="<?php echo !empty($setting_database->setting_database_id) ? $setting_database->setting_database_id : ''; ?>">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label text-right" for="">ตั้งชื่อไฟล์</label>
                <div class="col-sm-5">
                    <input value="" type="text" class="form-control m-input " name="export_name" placeholder="ระบุ">
                    <textarea id="query_select_tables" class="form-control m-input" name="query_select_tables" rows="10" col="5" placeholder="ระบุ" hidden><?php echo !empty($textarea_select_tables) ? $textarea_select_tables : ''; ?></textarea>
                </div>
            </div>
            <br>
            <div class="table-responsive">
                <table id="data-lists" class="table table-hover dataTable" width="100%">
                    <thead>
                        <?php
                        if (!empty($table_thead)) :
                        ?>
                            <tr>
                                <!-- <th style="text-align: left;"><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th> -->
                                <?php
                                foreach ($table_thead as $thead) :
                                ?>
                                    <th style="text-align: left;"><?php echo !empty($thead) ? $thead : ''; ?></th>
                                <?php
                                endforeach;
                                ?>
                            </tr>
                        <?php
                        else :
                        ?>
                            <th style="text-align: left;">FieLd 1</th>
                            <th style="text-align: left;">FieLd 2</th>
                            <th style="text-align: left;">FieLd 3</th>
                            <th style="text-align: left;">FieLd 4</th>
                            <th style="text-align: left;">FieLd 5</th>
                            <th style="text-align: left;">FieLd 6</th>
                            <th style="text-align: left;">FieLd 7</th>
                        <?php
                        endif;
                        ?>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($table_thead)) :
                            if (!empty($table_result)) :
                                foreach ($table_result as $result) :
                        ?>
                                    <tr>
                                        <!-- <td><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></td> -->
                                        <?php
                                        foreach ($table_thead as $thead) :
                                            $thead = !empty($thead) ? $thead : "";
                                        ?>
                                            <td><?php echo !empty($result->$thead) ? $result->$thead : ''; ?></td>
                                        <?php
                                        endforeach;
                                        ?>
                                    </tr>
                        <?php
                                endforeach;
                            endif;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>

            </form>

        </div>
    </div>
    <?php echo form_close() ?>
</div>