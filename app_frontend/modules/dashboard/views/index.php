<!--Begin::Section-->
<div class="row">
    <div class="col-xl-8">
        <div class="m-portlet m-portlet--mobile ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Log run job
                        </h3>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__body"> 
                <!--begin: Datatable --> 
                <table id="data-list-dashboard" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: left;">วันที่สร้าง</th>
                            <th style="text-align: left;">database name</th>
                            <th style="text-align: left;">สถานะ</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i = 0; $i < 5; $i++):
                            $name = 'Oracle';
                            if($i == 2 || $i == 4):
                                $name = 'MySql';
                            endif;
                        ?>
                        <tr>
                            <td>2020-08-25 23:59:00 น.</td>
                            <td><?=$name?></td>
                            <td><span class="m-badge m-badge--success m-badge--wide">success</span></td> 
                        </tr>
                         <?php
                         endfor;
                         ?>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <div class="col-xl-4">

        <!--begin:: Widgets/Audit Log-->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Job
                        </h3>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_widget4_tab1_content">
                        <div class="m-scrollable" data-scrollable="true" style="overflow: hidden;">
                            <div class="m-list-timeline m-list-timeline--skin-light">
                                <div class="m-list-timeline__items"> 
                                    <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
                                        <span class="m-list-timeline__text">รันทุกๆ 1 วัน <span class="m-badge m-badge--success m-badge--wide">เปิด</span></span>
                                        <span class="m-list-timeline__time">23:59 น.</span>
                                    </div> 
                                    <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
                                        <span class="m-list-timeline__text">รันทุกๆ 1 สัปดาห์ <span class="m-badge m-badge--danger m-badge--wide">ปิด</span></span>
                                        <span class="m-list-timeline__time">23:59 น.</span>
                                    </div> 
                                    <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                        <span href="" class="m-list-timeline__text">รันทุกๆ 1 เดือน <span class="m-badge m-badge--danger m-badge--wide">ปิด</span></span>
                                        <span class="m-list-timeline__time">23:59 น.</span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <!--end:: Widgets/Audit Log-->
    </div>
</div>

<!--End::Section-->