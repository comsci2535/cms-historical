<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_folder extends MX_Controller {

    private $_title = 'ตั้งค่าโฟลเดอร์ไฟล์';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับตั้งค่าโฟลเดอร์ไฟล์';
    private $_grpContent = 'setting_folder';
    private $_permission;

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if (!$this->_permission && !$this->input->is_ajax_request()) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        
        $this->load->model('config_m');
    }

    public function index() {
        $this->load->module('template');

        $type = 'setting_folder';
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        }
        $data['info'] = $temp;
        $data['frmAction'] = site_url("{$this->router->class}/update/{$type}");

        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/{$this->router->method}";
        
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array("โฟลเดอร์ไฟล์", site_url("{$this->router->class}/{$this->router->method}"));
        
        $this->template->layout($data);
    }

    public function update($type) {

        $input = $this->input->post();
        $input['type'] = $type;
        $value = $this->_build_data($input);
        $result = $this->config_m->update($value, $type);

        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }

    private function _build_data($input) {
        $value = array();
        $i = 0;
        foreach ($input as $key => $rs) {
            $i++;
            if ( $key != "type") {
                $value[$i]['value'] = html_escape($rs);
                $value[$i]['variable'] = html_escape($key);
                $value[$i]['type'] = html_escape($input['type']);
                $value[$i]['lang'] = config_item('language_abbr');
                $value[$i]['updateDate'] = db_datetime_now();
            }

        }
        return $value;
    }

}
