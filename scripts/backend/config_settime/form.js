"use strict";

$(document).ready(function () {

    $(document).on('click', '.input-check-box', function(){
        $('.input-check-box').prop('checked', false);
        $(this).prop('checked', true);
    })

    $('#file').fileinput({
        initialPreview: [file_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { 
            type : "text",
            downloadUrl: file_image, 
            extra: { 
                id: file_id, 
                csrfToken: get_cookie('csrfCookie') 
            } 
        }],
        deleteUrl: deleteUrl,
        maxFileCount: 1, 
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: true,
        // maxFileSize:20240,
        initialPreviewFileType: 'image',
        browseOnZoneClick: true,
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: { // configure your icon file extensions 
            'bah': '<i class="glyphicon glyphicon-file text-danger"></i>', 
            'sh': '<i class="glyphicon glyphicon-file text-danger"></i>',   
        },
        previewFileExtSettings: { // configure the logic for determining icon file extensions
            'bah': function(ext) {
                return ext.match(/(bah|)$/i);
            },
            'sh': function(ext) {
                return ext.match(/(sh|)$/i);
            },},
        allowedFileExtensions: ["bat","sh"],
        browseLabel: "เลือกไฟล์",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop icon here …",

      })
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
