<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
    private $_title = 'แผงควบคุม';
    private $_pageExcerpt = 'แสดงรายการโดยรวมของระบบ';
    private $_grpContent = 'grpContent';
    
    public function __construct() {
        parent::__construct();
      // $this->load->model('policy/policy_m');
    }

    public function index() {
        $this->load->module('template');
        
        if ( $this->session->firstTime ):
            Modules::run('utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        endif; 
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
             
        $this->template->layout($data);
    }

    
}
