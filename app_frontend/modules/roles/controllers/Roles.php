<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MX_Controller {

    private $_title = 'กลุ่มผู้ดูแลระบบ';
    private $_pageExcerpt = 'การจัดการกลุ่มผู้ดูแลระบบ';
    private $_grpContent = 'roles';
    private $_permission;

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->model('roles_m');
    }
    
    public function index() {
        $this->load->module('template');        
        // toobar
        $action[1][] = action_refresh(base_url("{$this->router->class}"));
        $action[1][] = action_add(base_url("{$this->router->class}/create"));
        $action[2][] = action_trash_multi("{$this->router->class}/destroy");
        $action[2][] = action_trash_view(base_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->roles_m->get_rows($input);
        $infoCount = $this->roles_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->role_id);
            $action = array();
            $action = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->name;
            $column[$key]['remark'] = $rs->remark;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = $action;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() 
    {
        $this->load->module('template');
        
        $info = $this->roles_m->get_module();
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->moduleId];
            $thisref['moduleId'] = $rs->moduleId;
            $thisref['parentId'] = $rs->parentId;
            $thisref['title'] = $rs->title;
            $thisref['icon'] = $rs->icon;
            $thisref['type'] = $rs->type;
            $thisref['hasChild'] = false;
            if ( $rs->parentId != 0 ) {
                $refs[$rs->parentId]['hasChild'] = true;
                $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
            } else {
                $module[$rs->moduleId] = &$thisref;
            }
        }
        //arr($module);
        $data['module'] = $module;

        $permission = $this->roles_m->get_permission();
        $data['permission'] = $permission->result();
        
        $data['frmAction'] = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', base_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->roles_m->insert($value);
        $value_permission = $this->_build_data_permission($result,$input);
        $this->roles_m->update_permission($result, $value_permission);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id=0) {
        $this->load->module('template');
        
        $id = decode_id($id);
        $info = $this->roles_m->get_module();
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->moduleId];
            $thisref['moduleId'] = $rs->moduleId;
            $thisref['parentId'] = $rs->parentId;
            $thisref['title'] = $rs->title;
            $thisref['icon'] = $rs->icon;
            $thisref['type'] = $rs->type;
            $thisref['hasChild'] = false;
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['hasChild'] = true;
                $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
            } else {
                $module[$rs->moduleId] = &$thisref;
            }
        }
        $info = $this->roles_m->get_row($id);
        $data['info'] = $info;
        // if ( $info->policy ) {
        //     $policy = json_decode($info->policy);
        //     foreach ($policy as $key => $rs) 
        //         foreach ( $rs as $key2 => &$rs2 )
        //             if ($rs2 == 0) unset($rs->{$key2});
        // } else {
        //     $policy = (object)array();
        // }
        $policy=$this->roles_m->get_permission_role($id)->result_array();
        $policy_ = array();
        if(!empty($policy)){ 
            foreach ($policy as $key => $value) {
                $policy_[$value['moduleId']][] = $value['permission_id'];
            }
        }
        $data['policy'] = $policy_;
        //arr($policy_);exit();
        $data['module'] = $module;
        $permission = $this->roles_m->get_permission();
        $data['permission'] = $permission->result();

        $data['frmAction'] = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', base_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->roles_m->update($id, $value);
        $value_permission = $this->_build_data_permission($id,$input);
        $this->roles_m->update_permission($id, $value_permission);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['name'] = $input['name'];
        $value['remark'] = $input['remark'];
        //$value['policy'] = json_encode($input['policy']);
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['create_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['update_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    private function _build_data_permission($role_id,$input) {
        
        if ( isset($input['policy'])) {
            foreach ( $input['policy'] as  $key => &$rs ){
                foreach ($rs as $key2 => $rs2) {
                    $value[] = [
                        'role_id' => $role_id,
                        'moduleId' => $key,
                        'permission_id' => $key2
                    ];
                }
            }
             
        } 

        return $value;
    }
    
    public function trash() {
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(base_url("{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", base_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->roles_m->get_rows($input);
        $infoCount = $this->roles_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->role_id);
            $action = array();
            $action[] = table_restore("{$this->router->class}/restore");
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->name;
            $column[$key]['remark'] = $rs->remark;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = $action;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    public function destroy()
    {
        $input = $this->input->post();
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            foreach ($input['id'] as &$rs) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['update_by'] = $this->session->users['user_id'];
            $result = false;
            
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $result = $this->roles_m->update_in($input['id'], $value);
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        $input = $this->input->post();
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            foreach ($input['id'] as &$rs) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['update_by'] = $this->session->users['user_id'];
            $result = false;
            
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->roles_m->update_in($input['id'], $value);
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        $input = $this->input->post();
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            foreach ($input['id'] as &$rs) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['update_by'] = $this->session->users['user_id'];
            $result = false;
            
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->roles_m->update_in($input['id'], $value);
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }     
    
    public function action($type=""){
        $input = $this->input->post();
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            foreach ($input['id'] as &$rs) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['update_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->roles_m->update_in($input['id'], $value);
            }
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
    public function dropdown() {
        $info = $this->roles_m->get_dropdown();
        $temp[''] = 'เลือกรายการ';
        foreach ( $info->result() as $rs ) $temp[$rs->role_id] = $rs->name;
        return $temp;
    }
    
    public function login($id=0) {

        $policy_arr=$this->roles_m->get_permission_role($id)->result_array();
        $policy_ = array();
        if(!empty($policy_arr)){ 
            foreach ($policy_arr as $key => $value) {
                $policy_[$value['moduleId']][] = $value['permission_id'];
            }
        }
        $policy = $policy_;

        return $policy;
    }
    
}
